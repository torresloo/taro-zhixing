export default {
  navigationBarTitleText: '城市选择',
  // 配置顶部导航栏的背景颜色
  navigationBarBackgroundColor: '#0066e6',
  // 配置顶部导航栏文本颜色
  navigationBarTextStyle: 'white'
}
