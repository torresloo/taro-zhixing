import { PureComponent } from "react";
import Taro from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from 'react-redux';
import "./index.scss";

@connect(({ flightIndex }) => ({
  flightIndex
}))
class CityItem extends PureComponent {

  handleClickCity = (cityInfo) => {
    const { cityType } = this.props.flightIndex;
    const { cityId, cityName, airportName } = cityInfo;
    this.props.dispatch({
      type: "flightIndex/updateState",
      payload: cityType === "depart" ? {
        dptCityId: cityId,
        dptAirportName: airportName,
        dptCityName: cityName
      } : {
        arrCityId: cityId,
        arrAirportName: airportName,
        arrCityName: cityName
      }
    })
    Taro.navigateBack()
  }

  render() {
    const { cityList, label } = this.props;
    return (
      // 定义一个id作为锚点
      <View className="list-item" id={label}>
        <Text className="label">{label}</Text>
        {cityList?.map((item) => {
          return (
            <View key={item.id} className="name" onClick={() => this.handleClickCity(item)}>
              <Text>{`${item.cityName}（${item.airportName}）`}</Text>
            </View>
          );
        })}
      </View>
    );
  }
}

export default CityItem;
