import { PureComponent } from "react";
import { View } from "@tarojs/components";
import { connect } from "react-redux";
import Taro from "@tarojs/taro";
import { AtCalendar } from "taro-ui";
import { MAX_DATE, MIN_DATE } from "@/utils/constant";

import "./calendar.scss";

@connect(({ flightIndex }) => ({
  flightIndex,
}))
class CalendarPage extends PureComponent {

  handleSelectDate = ({ value: { start } }) => {
    this.props.dispatch({
      type: 'flightIndex/updateState',
      payload: {
        dptDate: start
      }
    })
    Taro.navigateBack();
  }

  render() {
    const {
      flightIndex: { dptDate },
    } = this.props;
    return (
      <View className="calendar-page">
        <AtCalendar
          currentDate={dptDate}
          minDate={MIN_DATE}
          maxDate={MAX_DATE}
          onSelectDate={this.handleSelectDate}
        ></AtCalendar>
      </View>
    );
  }
}

export default CalendarPage;
