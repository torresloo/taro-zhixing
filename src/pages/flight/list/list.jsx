import { PureComponent } from "react";
import Taro, { getCurrentInstance } from "@tarojs/taro";
import { ScrollView, View } from "@tarojs/components";
import dayjs from "dayjs";
import { MAX_DATE, MIN_DATE } from "@/utils/constant";
import { weekDay } from "@/utils/utils";

import "./list.scss";

class FlightListPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dateList: this.formatDateList(),
      flightData: {}, // 航班参数
    };
    this.initFlightList = [];
  }

  componentDidMount() {
    const {
      params: {
        dptCityId,
        dptCityName,
        dptAirportName,
        arrCityId,
        arrCityName,
        arrAirportName,
        cityType,
        dptDate,
      },
    } = getCurrentInstance().router;
    this.setState({
      flightData: {
        dptCityId,
        dptCityName,
        dptAirportName,
        arrCityId,
        arrCityName,
        arrAirportName,
        cityType,
        dptDate,
      },
    });
    Taro.setNavigationBarTitle({
      title: `${dptCityName} - ${arrCityName}`,
    });
  }

  // 格式化日期列表
  formatDateList = () => {
    let minStr = dayjs(MIN_DATE).valueOf();
    const maxStr = dayjs(MAX_DATE).valueOf();
    const dayStr = 1000 * 60 * 60 * 24;
    let res = [];
    for (; minStr <= maxStr; minStr += dayStr) {
      res.push({
        dateStr: dayjs(minStr).format("YYYY-MM-DD"),
        day: dayjs(minStr).format("M-DD"),
        week: weekDay(minStr),
      });
    }
    return res;
  };

  render() {
    const { dateList, flightData } = this.state;
    const { dptDate } = flightData;
    return (
      <View className="list-container">
        <View className="calendar-list">
          <ScrollView
            scrollX
            scrollWithAnimation
            scrollIntoView={`date-${dptDate}`}
            className="calendar-scroll-list"
          >
            {dateList.map((date) => {
              return (
                <View
                  key={date.dateStr}
                  className={`item ${date.dateStr === dptDate ? "cur" : ""}`}
                  id={`date-${date.dateStr}`}
                >
                  <View className="date">{date.day}</View>
                  <View className="week">{date.week}</View>
                </View>
              );
            })}
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default FlightListPage;
