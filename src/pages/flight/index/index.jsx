import { PureComponent } from "react";
import Taro from "@tarojs/taro";
import {
  View,
  SwiperItem,
  Swiper,
  Image,
  Text,
  Button,
} from "@tarojs/components";
import { connect } from "react-redux";
import dayjs from "dayjs";
import Tab from "@/components/Tab";
import NoExploit from "@/components/NoExploit";
import { adsReq } from "@/utils/api";
import tools from "@/utils/tools";

import "./index.scss";

const FLIGHT_TAB = [
  {
    label: "单程",
    id: 0,
  },
  {
    label: "多程",
    id: 1,
  },
  {
    label: "往返",
    id: 2,
  },
];

// const BANNER_IMGS = [
//   "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F05%2F87%2F39%2F955c8a609be2216.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1644334476&t=903e4eb59d4299c15d9ae8e6df6028e9",
//   "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.jnjsdq.cn%2Fimg.php%3Fpic.90sjimg.com%2Fback_pic%2F00%2F04%2F27%2F49%2F573eaec99158eea532234fc4918f66ff.jpg&refer=http%3A%2F%2Fwww.jnjsdq.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1644334476&t=0280ef809ed86f903729113140872047"
// ]
@connect(({ flightIndex }) => ({
  flightIndex,
}))
export default class FlightIndex extends PureComponent {
  state = {
    adList: [],
  };

  componentDidMount() {
    this.getAds();
    this.getLocationInfo();
  }

  handleChangeTab = (id) => {
    console.log(id);
  };

  getAds = async () => {
    try {
      const data = await adsReq();
      this.setState({ adList: data.result ?? [] });
    } catch (error) {
      console.log(error);
    }
  };

  chooseFlightCity = (type) => {
    this.props.dispatch({
      type: "flightIndex/updateState",
      payload: {
        cityType: type,
      },
    });
    Taro.navigateTo({ url: "/pages/airportList/airportList" });
  };

  chooseFlightDate = () => {
    Taro.navigateTo({ url: "/pages/calendar/calendar" });
  };

  getLocationInfo = () => {
    Taro.getLocation({
      type: "gcj02",
    })
      .then((res) => {
        const { latitude, longitude } = res;
        this.getCity({ latitude, longitude });
      })
      .catch(() => {
        tools.showToast("位置获取失败~");
      });
  };

  getCity = ({ latitude, longitude }) => {
    Taro.request({
      url: `https://apis.map.qq.com/ws/geocoder/v1/?key=JKLBZ-WN3K4-HFSU6-DB5UU-2FGCS-CLB4J&location=${latitude},${longitude}`,
    })
      .then((res) => {
        const { data } = res;
        const cityInfo = data?.result?.ad_info ?? {};
        this.props.dispatch({
          type: "flightIndex/updateState",
          payload: {
            dptCityName: cityInfo.city ?? "上海",
            dptCityId: cityInfo.city_code ?? 2,
          },
        });
      })
      .catch(() => {
        tools.showToast("位置获取失败~");
      });
  };

  linkToList = () => {
    // const {
    //   dptCityId,
    //   dptCityName,
    //   dptAirportName,
    //   arrCityId,
    //   arrCityName,
    //   arrAirportName,
    //   cityType,
    //   dptDate,
    // } = this.props.flightIndex;
    tools.navigateTo({ url: '/pages/flight/list/list', params: this.props.flightIndex })
  };

  render() {
    const { arrCityName, dptCityName, dptDate } = this.props.flightIndex;
    const { adList } = this.state;
    return (
      <View className="flight-container">
        <View className="flight-top">
          <Tab
            className="flight-index-tab"
            tabList={FLIGHT_TAB}
            onChangeTab={this.handleChangeTab}
          >
            <SwiperItem>
              <View className="item station">
                <View
                  className="cell from"
                  onClick={() => this.chooseFlightCity("depart")}
                >
                  {dptCityName}
                </View>
                <Text className="iconfont icon-zhihuan"></Text>
                <View
                  className="cell to"
                  onClick={() => this.chooseFlightCity("arrive")}
                >
                  {arrCityName}
                </View>
              </View>
              <View className="item date" onClick={this.chooseFlightDate}>
                {dayjs(dptDate).format("M月D日")}
              </View>
              <Button className="search-btn" onClick={this.linkToList}>机票查询</Button>
            </SwiperItem>
            <SwiperItem>敬请期待</SwiperItem>
            <SwiperItem>
              <NoExploit className="no-data" />
            </SwiperItem>
          </Tab>
        </View>

        <Swiper className="advs-banner-bd" autoplay circular interval={3000}>
          {adList.map((item) => {
            return (
              <SwiperItem key={item.id} className="item">
                <Image className="img" src={item.imgUrl} />
              </SwiperItem>
            );
          })}
        </Swiper>
      </View>
    );
  }
}
