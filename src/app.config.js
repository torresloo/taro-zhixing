export default {
  // 注册路由
  pages: [
    'pages/index/index',
    "pages/order/order",
    "pages/airportList/airportList",
    "pages/calendar/calendar",
    "pages/flight/list/list"
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '智行',
    navigationBarTextStyle: 'black'
  },
  // 配置底部 tab 栏
  tabBar: {
    // 未选中的字体颜色
    color: "#7f8389",
    // 选中的字体颜色
    selectedColor: "#5495e6",
    // tabbar顶部边线的颜色
    borderStyle: 'black',
    // tabbar 背景颜色
    backgroundColor: '#fff',
    // 配置tabbar每一项，至少两项，最多五项
    list: [
      {
        pagePath: 'pages/index/index',
        iconPath: 'assets/images/index-unselected.png',
        selectedIconPath: 'assets/images/index-selected.png',
        text: '首页'
      },
      {
        pagePath: 'pages/order/order',
        iconPath: 'assets/images/order-unselected.png',
        selectedIconPath: 'assets/images/order-selected.png',
        text: '订单'
      }
    ]
  },
  permission: {
    "scope.userLocation": {
      desc: "为了更好的服务体验，我们希望获取您的位置"
    }
  }
}
