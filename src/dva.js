import { create } from 'dva-core';

let app; // dva 实例
let store; // 项目所有的state树的对象
let dispatch;

const createApp = (opts) => {
  app = create(opts);

  // 确保只注册一次 model
  if (!global.registered) {
    opts.models.forEach(model => app.model(model));
  }
  global.registered = true;

  // 运行程序
  app.start();

  store = app._store;
  app.getStore = () => store;
  dispatch = store.dispatch;
  app.dispatch = dispatch;

  return app;
}

export default createApp;
