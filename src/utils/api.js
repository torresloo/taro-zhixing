import tools from './tools';

export const adsReq = (data) => tools.request({
  url: '/ads/advertising',
  params: data
})

export const airportCityListReq = (data) => tools.request({
  url: '/city/airportList',
  params: data
})
