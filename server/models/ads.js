const express = require("express")
const sqlQuery = require("../mysql")

const router = express.Router()

// const imgLists = [
//   'https://images3.c-ctrip.com/ztrip/flightbanner/student_authentication/%25E6%259C%25BA%25E7%25A5%25A8%25E9%25A6%2596%25E9%25A1%25B5@3x.png',
//   'https://images3.c-ctrip.com/ztrip/flightbanner/yaoxinyouli/app%25E9%25A6%2596%25E9%25A1%25B5.png'
// ]

// const createTable = async () => {
//   try {
//     const  createTableSql = `
//       create table if not exists ads (
//         id int auto_increment,
//         imgUrl char(255) not null,
//         primary key (id)
//       ) engine=innodb;
//     `;
//     await sqlQuery(createTableSql);
//     for (let item of imgLists) {
//       const insertSql = `insert into ads(id, imgUrl) values (null, '${item}')`;
//       await sqlQuery(insertSql);
//     }
//   } catch(e) {
//     console.log(e)
//   }
// }
// createTable()

router.get("/advertising", async (req, res) => {
  const strSql = `select * from ads`;
  try {
    const result = await sqlQuery(strSql);
    res.send({
      code: 1,
      message: '请求成功',
      result,
    })
  } catch (err) {
    res.send({
      code: -1,
      message: '失败'
    })
  }
})

module.exports = router
